import { Params } from '@models/interfaces/params';
import { Image } from '@models/spotify/image';
import { Tracks } from '@models/spotify/tracks';
import { UserPublic } from '@models/spotify/user-public';

export class Playlist {
  collaborative: boolean;
  external_urls: Params;
  href: string;
  id: string;
  images: Image[];
  name: string;
  owner: UserPublic;
  public: boolean;
  snapshot_id: string;
  tracks: Tracks;
  type: string;
  uri: string;

  constructor(data: object = {}) {
    this.collaborative = data['collaborative'];
    this.external_urls = data['external_urls'];
    this.href = data['href'];
    this.id = data['id'];
    this.images = (data['images'] || []).map(image => new Image(image));
    this.name = data['name'];
    this.owner = new UserPublic(data['owner']);
    this.public = data['public'];
    this.snapshot_id = data['snapshot_id'];
    this.tracks = new Tracks(data['tracks']);
    this.type = data['type'];
    this.uri = data['uri'];
  }
}
