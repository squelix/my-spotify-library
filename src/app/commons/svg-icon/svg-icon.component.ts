import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss'],
})
export class SvgIconComponent {
  @Input()
  icon: string;

  @Input()
  label = '';

  @Input()
  classes = '';

  @Input()
  width: string;

  @Input()
  height: string;

  @Input()
  viewBox: string;

  @Input()
  preserveAspectRatio = 'xMinYMax meet';
}
