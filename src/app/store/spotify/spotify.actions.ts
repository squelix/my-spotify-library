import { UserPrivate } from '@models/spotify/user-private';

export class RedirectForToken {
  static readonly type = '[Spotify] Redirect For Token';
}

export class Init {
  static readonly type = '[Spotify] Init';
  constructor(public accessToken: string) {}
}

export class GetMyInfo {
  static readonly type = '[Spotify] Get My Info';
}

export class GetMyInfoSuccess {
  static readonly type = '[Spotify] Get My Info Success';
  constructor(public user: UserPrivate) {}
}

export class GetMyInfoFailed {
  static readonly type = '[Spotify] Get My Info Failed';
  constructor(public error: any) {}
}
