# My Spotify Library


This project is based on [Angular](https://angular.io/).

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/237aae3582464690bb609ae499700d73)](https://www.codacy.com/app/mickael.depardon/my-spotify-library?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=squelix/my-spotify-library&amp;utm_campaign=Badge_Grade)

## Development

1. Run `yarn start`
2. Visit `http://localhost:4200/`

Use [Angular CLI](https://github.com/angular/angular-cli/wiki)'s [`ng generate` command](https://github.com/angular/angular-cli/wiki/generate) to generate new components, directives, modules, etc.

## Build

The following build commands are available:

- `yarn build` - build the app for production in the default locale (French).
- `yarn build --configuration=(production|qa3|qa4|branch)` - build the app for a specific environment:
  - `production` (default) - production APIs, full optimisations, fail on missing translations;
  - `qa3`: QA3 APIs, same optimisations as `production`, ignore missing translations;
  - `qa4`: QA4 APIs, fewer optimisations than `production` for debugging purposes;
  - `branch`: QA3 APIs, same optimisations as `qa4`.
- `yarn build:lorem` - build a fake localised version of the app for testing purposes.

The build outputs are stored in the `dist` folder and organised by locale.

## Quality

### Testing

Jest is used for unit and feature testing. The following commands are available ([amongst others](https://jestjs.io/docs/en/cli)): 

- `yarn test` - run all test suites with Jest.
- `yarn test <filter>` - run the test suites that match the filter.
- `yarn test --watch` - test and watch modified files.
- `yarn test --watch <filter>` - test and watch files that match the filter.
- `yarn test --watchAll` - test and watch all files.
- `yarn test --onlyChanged` - run the test suites that relate only to modified files without watching (see also `--lastCommit`).
- `yarn test --coverage` - run all test suites and output a coverage report.
- `yarn test --coverage --findRelatedTests <filter>` - test specific files and all files they relate to, and output a coverage report.
- `yarn test --verbose` - log the test cases.

### Linting

The following linting commands are available:

- `yarn lint:ts` - lint TypeScript files with Angular CLI and the shared configuration [@wizbii/tslint-config](https://bitbucket.org/wizbii/wizbii-tslint-config/), which uses TSLint's core rules.
- `yarn lint:app` - lint application files (i.e. not specs) with Angular CLI and the shared configuration [@wizbii/tslint-config-angular](https://bitbucket.org/wizbii/wizbii-tslint-config-angular/), which uses rules from codelyzer and tslint-sonarts.
- `yarn lint:spec` - compile spec files without emitting to make sure there are no TypeScript errors (Jest doesn't report compilation errors). Note that application files are compiled as well as a side effect, but the performance impact is minimal.
- `yarn lint:scss` - lint SCSS files with [stylelint](https://stylelint.io/).
- `yarn lint:pretty` - check that TypeScript files respect Prettier formatting.

To run all the commands at once, use `yarn lint`. This command is used on the CI and by the Git pre-push hook.
For effiency, it runs the linting commands in parallel and stops early if one fails. You can change the latter behaviour by passing [npm-run-all](https://github.com/mysticatea/npm-run-all/blob/master/docs/run-p.md)'s `--continue-on-error` option.

#### Fixing

The following commands are available to fix linting issues:

- `yarn lint:ts --fix`
- `yarn lint:app --fix`
- `yarn lint:scss --fix`
- `yarn lint:pretty --write`

Your code editor should be configured to fix TSLint errors and format files with Prettier on save (like running `yarn lint:ts --fix && yarn lint:pretty --write` but on a specific file).

> With VSCode, everything is configured for you but you still need to install the recomended extensions (cf. `.vscode/extensions.json`).
