import { Playlist } from '@models/spotify/playlist';

export class GetMyPlaylists {
  static readonly type = '[Spotify] Get My Playlists';
}

export class GetMyPlaylistsSuccess {
  static readonly type = '[Spotify] Get My Playlists Success';
  constructor(
    public items: Playlist[],
    public limit: number,
    public next: string,
    public offset: number,
    public previous: string,
    public total: number
  ) {}
}

export class GetMyPlaylistsFailed {
  static readonly type = '[Spotify] Get My Playlists Failed';
  constructor(public error: any) {}
}
