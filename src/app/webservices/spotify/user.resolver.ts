import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { SpotifyState } from '@store/spotify/spotify.state';
import { GetMyInfo } from '@store/spotify/spotify.actions';
import { UserPrivate } from '@models/spotify/user-private';

@Injectable()
export class UserResolver implements Resolve<UserPrivate> {
  constructor(private readonly store: Store) {}

  resolve(): Observable<UserPrivate> {
    this.initUser();

    return this.waitForUserToLoad();
  }

  private initUser(): void {
    this.store.select(SpotifyState.user).subscribe(user => {
      if (!user) {
        this.store.dispatch(new GetMyInfo());
      }
    });
  }

  private waitForUserToLoad(): Observable<UserPrivate> {
    return this.store.select(SpotifyState.user).pipe(
      filter(user => !!user),
      take(1)
    );
  }
}
