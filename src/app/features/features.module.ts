import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FeaturesRoutingModule } from '@features/features-routing.module';
import { SpotifyStoreModule } from '@store/spotify/spotify.store.module';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';

@NgModule({
  imports: [CommonModule, FeaturesRoutingModule, SpotifyStoreModule],
  providers: [SpotifyWebservice],
  declarations: [],
})
export class FeaturesModule {}
