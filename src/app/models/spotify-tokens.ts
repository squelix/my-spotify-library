export class SpotifyTokens {
  accessToken: string;

  constructor(data: object = {}) {
    this.accessToken = data['accessToken'];
  }
}
