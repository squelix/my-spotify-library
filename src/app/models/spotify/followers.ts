export class Followers {
  href: string;
  total: number;

  constructor(data: object = {}) {
    this.href = data['href'];
    this.total = data['total'];
  }
}
