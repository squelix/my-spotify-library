import { Injectable } from '@angular/core';
import { DataStorage } from '@models/interfaces/data-storage.interface';

@Injectable()
export class DataStorageService implements DataStorage {
  put(key: string, value: any): void {
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  get(key: string, Factory?: { new (data: object): any }): any | undefined {
    if (!window.localStorage.getItem(key)) {
      return;
    }

    const data = JSON.parse(window.localStorage.getItem(key));
    if (!Factory) {
      return data;
    }

    if (Array.isArray(data)) {
      return data.map(item => new Factory(item));
    }

    return new Factory(data);
  }

  remove(key: string): void {
    if (window.localStorage.getItem(key)) {
      window.localStorage.removeItem(key);
    }
  }

  clear(): void {
    while (window.localStorage.length > 0) {
      window.localStorage.removeItem(window.localStorage.key(0));
    }
  }
}
