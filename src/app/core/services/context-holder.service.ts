import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { DataStorage } from '@models/interfaces/data-storage.interface';
import { SpotifyTokens } from '@models/spotify-tokens';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable()
export class ContextHolderService {
  private static readonly KEY_STORAGE_TOKENS = `${environment.applicationId}_tokens`;
  private readonly spotifyTokensSubject: Subject<SpotifyTokens>;

  constructor(private readonly dataStorageService: DataStorage) {
    this.spotifyTokensSubject = new BehaviorSubject(
      this.dataStorageService.get(ContextHolderService.KEY_STORAGE_TOKENS, SpotifyTokens)
    );
  }

  // Gestion de l'utilisateur connecté
  get tokens$(): Observable<SpotifyTokens> {
    return this.spotifyTokensSubject.asObservable();
  }

  set tokens$(tokens$: Observable<SpotifyTokens>) {
    tokens$.subscribe(tokens => {
      if (tokens) {
        this.dataStorageService.put(ContextHolderService.KEY_STORAGE_TOKENS, tokens);
      } else {
        this.dataStorageService.remove(ContextHolderService.KEY_STORAGE_TOKENS);
      }

      this.spotifyTokensSubject.next(tokens);
    });
  }
}
