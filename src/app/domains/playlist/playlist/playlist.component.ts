import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Image } from '@models/spotify/image';
import { Playlist } from '@models/spotify/playlist';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlaylistComponent implements OnInit {
  @Input()
  playlist: Playlist;

  image: Image = new Image({
    url:
      'https://store-images.s-microsoft.com/image/apps.52691.13571498826857201.5bda3835-53b1-4825-ba61-ae335fbbbdd8.2995406a-229c-4a24-ae8a-39af0e795554?mode=scale&q=90&h=270&w=270&background=%231ed760',
    width: 640,
    height: 640,
  });

  ngOnInit(): void {
    this.image = this.playlist.images.find(image => image.width === 640);
  }
}
