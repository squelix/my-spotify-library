// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  applicationId: 'my-spotify-library',
  prod: false,
  dev: true,
  name: 'dev',
  spotify: {
    clientId: 'f80f215660cf4d79b44ee5fee4b21e77',
    secret: '5efdb1fdc5d3457185dc619b392e5286',
  },
  api: {
    spotify: '/api/spotify',
  },
};
