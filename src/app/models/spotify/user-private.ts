import { UserPublic } from '@models/spotify/user-public';

export class UserPrivate extends UserPublic {
  birthdate: string;
  country: string;
  email: string;
  product: string;

  constructor(data: object = {}) {
    super(data);

    this.birthdate = data['birthdate'];
    this.country = data['country'];
    this.email = data['email'];
    this.product = data['product'];
  }
}
