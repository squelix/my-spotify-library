import { buildPath } from '@commons/utils/routing.utils';
import { ContextHolderService } from '@core/services/context-holder.service';
import { FeaturesRoutingEnum } from '@features/features-routing.enum';
import { SpotifyTokens } from '@models/spotify-tokens';
import { UserPrivate } from '@models/spotify/user-private';
import { Navigate } from '@ngxs/router-plugin';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { PlaylistsState } from '@store/spotify/playlists/playlists.state';
import { GetMyInfo, GetMyInfoFailed, GetMyInfoSuccess, Init, RedirectForToken } from '@store/spotify/spotify.actions';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';
import { from } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

export interface SpotifyStateModel {
  error: any;
  user: UserPrivate;
}

const defaultState: SpotifyStateModel = {
  error: null,
  user: null,
};

@State<SpotifyStateModel>({
  name: 'spotify',
  defaults: defaultState,
  children: [PlaylistsState],
})
export class SpotifyState {
  @Selector()
  static user(state: SpotifyStateModel) {
    return state.user;
  }
  constructor(
    private readonly contextHolder: ContextHolderService,
    private readonly spotifyWebservice: SpotifyWebservice
  ) {}

  @Action(RedirectForToken)
  redirectForToken() {
    this.spotifyWebservice.redirectSpotify();
  }

  @Action(Init)
  init(ctx: StateContext<SpotifyStateModel>, action: Init) {
    const { accessToken } = action;

    this.contextHolder.tokens$ = from([
      new SpotifyTokens({
        accessToken,
      }),
    ]);

    return ctx.dispatch(new GetMyInfo());
  }

  @Action(GetMyInfo)
  getMyInfo(ctx: StateContext<SpotifyStateModel>) {
    return this.spotifyWebservice.getMyUserInfo().pipe(
      switchMap(user => ctx.dispatch(new GetMyInfoSuccess(user))),
      catchError(error => ctx.dispatch(new GetMyInfoFailed(error)))
    );
  }

  @Action(GetMyInfoSuccess)
  getMyInfoSuccess(ctx: StateContext<SpotifyStateModel>, action: GetMyInfoSuccess) {
    ctx.patchState({
      user: new UserPrivate(action.user),
    });
    return ctx.dispatch(new Navigate([buildPath(FeaturesRoutingEnum.Library)]));
  }

  @Action(GetMyInfoFailed)
  getMyInfoFailed(ctx: StateContext<SpotifyStateModel>, action: GetMyInfoFailed) {
    ctx.patchState({
      error: action.error,
    });
  }
}
