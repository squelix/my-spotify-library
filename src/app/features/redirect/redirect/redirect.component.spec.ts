import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ContextHolderService } from '@core/services/context-holder.service';
import { DataStorageService } from '@core/services/data-storage.service';
import { RedirectComponent } from '@features/redirect/redirect/redirect.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxsModule } from '@ngxs/store';
import { SpotifyStoreModule } from '@store/spotify/spotify.store.module';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';

jest.mock('@core/services/context-holder.service');
jest.mock('@core/services/data-storage.service');

describe('RedirectComponent', () => {
  let component: RedirectComponent;
  let fixture: ComponentFixture<RedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FontAwesomeModule,
        NgxsModule.forRoot([]),
        SpotifyStoreModule,
        HttpClientTestingModule,
      ],
      declarations: [RedirectComponent],
      providers: [DataStorageService, ContextHolderService, SpotifyWebservice],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
