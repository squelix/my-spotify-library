import { DataStorage } from '@models/interfaces/data-storage.interface';

export class DataStorageService implements DataStorage {
  private readonly store: object;

  constructor() {
    this.store = [];
  }

  put(key: string, value: any): void {
    this.store[key] = JSON.stringify(value);
  }

  get(key: string, Factory?: { new (data: object): any }): any | undefined {
    if (this.store[key] === undefined) {
      return;
    }

    const data = JSON.parse(this.store[key]);
    if (!Factory) {
      return data;
    }

    if (Array.isArray(data)) {
      return data.map(item => new Factory(item));
    }

    return new Factory(data);
  }

  remove(key: string): void {
    if (this.store[key]) {
      delete this.store[key];
    }
  }

  clear(): void {
    Object.keys(this.store).forEach(prop => {
      delete this.store[prop];
    });
  }
}
