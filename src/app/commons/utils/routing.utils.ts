export function buildPath(...paths: (string | undefined)[]): string {
  return `/${paths.join('/')}`;
}
