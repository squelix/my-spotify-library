import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeaturesRoutingEnum } from '@features/features-routing.enum';

const routes: Routes = [
  {
    path: FeaturesRoutingEnum.Home,
    loadChildren: '@features/home/home.module#HomeModule',
  },
  {
    path: FeaturesRoutingEnum.Library,
    loadChildren: '@features/library/library.module#LibraryModule',
  },
  {
    path: FeaturesRoutingEnum.Spotify,
    loadChildren: '@features/redirect/redirect.module#RedirectModule',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeaturesRoutingModule {}
