import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SvgIconModule } from '@commons/svg-icon/svg-icon.module';
import { LibraryRoutingModule } from '@features/library/library-routing.module';
import { LibraryComponent } from '@features/library/library/library.component';
import { UserResolver } from '@webservices/spotify/user.resolver';
import { PlaylistModule } from '@domains/playlist/playlist.module';

@NgModule({
  imports: [CommonModule, LibraryRoutingModule, SvgIconModule, PlaylistModule],
  declarations: [LibraryComponent],
  providers: [UserResolver],
})
export class LibraryModule {}
