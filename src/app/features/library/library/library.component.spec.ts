import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContextHolderService } from '@core/services/context-holder.service';
import { DataStorageService } from '@core/services/data-storage.service';
import { PlaylistModule } from '@domains/playlist/playlist.module';
import { LibraryComponent } from '@features/library/library/library.component';
import { NgxsModule, Store } from '@ngxs/store';
import { SpotifyStoreModule } from '@store/spotify/spotify.store.module';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';

jest.mock('@core/services/context-holder.service');
jest.mock('@core/services/data-storage.service');

const defaultState = {
  spotify: {
    error: null,
    user: null,

    playlists: {
      items: [],
      limit: 50,
      offset: 0,
      next: null,
      total: null,
      previous: null,
      error: null,
    },
  },
};

describe('LibraryComponent', () => {
  let component: LibraryComponent;
  let fixture: ComponentFixture<LibraryComponent>;
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlaylistModule, NgxsModule.forRoot([]), SpotifyStoreModule, HttpClientTestingModule],
      declarations: [LibraryComponent],
      providers: [DataStorageService, ContextHolderService, SpotifyWebservice],
    }).compileComponents();

    store = TestBed.get(Store);
    store.reset(defaultState);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
