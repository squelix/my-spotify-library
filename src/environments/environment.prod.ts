export const environment = {
  applicationId: 'my-spotify-library',
  prod: true,
  dev: false,
  name: 'prod',
  spotify: {
    clientId: 'f80f215660cf4d79b44ee5fee4b21e77',
    secret: '5efdb1fdc5d3457185dc619b392e5286',
  },
  api: {
    spotify: '/api/spotify',
  },
};
