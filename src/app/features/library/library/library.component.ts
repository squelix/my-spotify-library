import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Playlist } from '@models/spotify/playlist';
import { Select, Store } from '@ngxs/store';
import { GetMyPlaylists } from '@store/spotify/playlists/playlists.actions';
import { PlaylistsState } from '@store/spotify/playlists/playlists.state';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LibraryComponent {
  @Select(PlaylistsState.playlists)
  playlists$: Observable<Playlist[]>;

  constructor(private readonly store: Store) {
    this.store.dispatch(new GetMyPlaylists());
  }

  trackByPlaylist(playlist: Playlist) {
    return playlist.id;
  }
}
