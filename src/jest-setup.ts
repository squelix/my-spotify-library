// tslint:disable

import 'jest-preset-angular'; // import first

import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

let storage = {};

TestBed.configureTestingModule({
  imports: [NoopAnimationsModule],
});

(global as any).IntersectionObserver = jest.fn(() => ({
  observe: jest.fn(),
  unobserve: jest.fn(),
  disconnect: jest.fn(),
}));

Object.defineProperty(window, 'localStorage', {
  value: {
    get length() {
      return Object.keys(storage).length;
    },
    key: index => Object.keys(storage)[index],
    getItem: key => (key in storage ? storage[key] : null),
    setItem: (key, value) => (storage[key] = value || ''),
    removeItem: key => delete storage[key],
    clear: () => (storage = {}),
  },
});

window['open'] = jest.fn();

// Define unknown CSS properties used in Angular animations
['flex', 'willChange'].map(prop => {
  Object.defineProperty(document.body.style, prop, {
    value: () => ({
      enumerable: true,
      configurable: true,
    }),
  });
});
