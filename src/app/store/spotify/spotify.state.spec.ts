import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { ContextHolderService } from '@core/services/context-holder.service';
import { DataStorageService } from '@core/services/data-storage.service';
import { NgxsModule, Store } from '@ngxs/store';
import { SpotifyStateModel } from '@store/spotify/spotify.state';
import { SpotifyStoreModule } from '@store/spotify/spotify.store.module';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';

jest.mock('@core/services/context-holder.service');
jest.mock('@core/services/data-storage.service');

const defaultState: SpotifyStateModel = {
  error: null,
  user: null,
};

describe('SpotifyState', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([]), SpotifyStoreModule, HttpClientTestingModule],
      providers: [DataStorageService, ContextHolderService, SpotifyWebservice],
    }).compileComponents();

    store = TestBed.get(Store);
    store.reset(defaultState);
  }));

  afterEach(async(() => {
    store.reset(defaultState);
  }));

  it('should create', () => {
    expect(store).toBeTruthy();
  });
});
