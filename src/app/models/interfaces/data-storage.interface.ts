export abstract class DataStorage {
  abstract put(key: string, value: any): void;
  abstract get(key: string, factory?: { new (data: object): any }): any;
  abstract remove(key: string): void;
  abstract clear(): void;
}
