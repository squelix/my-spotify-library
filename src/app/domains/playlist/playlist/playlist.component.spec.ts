import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PlaylistComponent } from '@domains/playlist/playlist/playlist.component';
import { Image } from '@models/spotify/image';
import { Playlist } from '@models/spotify/playlist';

describe('PlaylistComponent', () => {
  let component: PlaylistComponent;
  let fixture: ComponentFixture<PlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistComponent);
    component = fixture.componentInstance;
    component.playlist = new Playlist({
      images: [
        new Image({
          width: 640,
        }),
      ],
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
