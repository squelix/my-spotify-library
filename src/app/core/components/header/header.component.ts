import { Component } from '@angular/core';
import { buildPath } from '@commons/utils/routing.utils';
import { ContextHolderService } from '@core/services/context-holder.service';
import { FeaturesRoutingEnum } from '@features/features-routing.enum';
import { RedirectRoutingEnum } from '@features/redirect/redirect-routing.enum';
import { faSpotify } from '@fortawesome/free-brands-svg-icons';
import { UserPrivate } from '@models/spotify/user-private';
import { Select } from '@ngxs/store';
import { SpotifyState } from '@store/spotify/spotify.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  redirectingUrl = buildPath(FeaturesRoutingEnum.Spotify, RedirectRoutingEnum.Redirecting);
  faSpotify = faSpotify;

  @Select(SpotifyState.user)
  user$: Observable<UserPrivate>;

  constructor(readonly contextHolder: ContextHolderService) {}
}
