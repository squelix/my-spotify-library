import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, filter, first, switchMap, take } from 'rxjs/operators';
import { SPOTIFY_TOKEN_SERVICE_TOKEN } from '@webservices/spotify-token-injection.token';
import { SpotifyTokens } from '@models/spotify-tokens';
import { SpotifyTokenService } from '@webservices/spotify-token.service';

@Injectable()
export class SpotifyTokenInterceptor implements HttpInterceptor {
  isRefreshingToken = false;
  tokenSubject: BehaviorSubject<SpotifyTokens> = new BehaviorSubject<SpotifyTokens>(null);

  constructor(@Inject(SPOTIFY_TOKEN_SERVICE_TOKEN) private readonly spotifyTokenService: SpotifyTokenService) {}

  addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: { Authorization: `Bearer ${token}` },
    });
  }

  // tslint:disable:cognitive-complexity
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.spotifyTokenService.getTokens().pipe(
      first(),
      switchMap((tokens: SpotifyTokens) => {
        if (tokens && !this.spotifyTokenService.blacklistRoutes.some(apiUrl => request.url.indexOf(apiUrl) !== -1)) {
          return next.handle(this.addToken(request, tokens.accessToken)).pipe(
            catchError(error => {
              if (error instanceof HttpErrorResponse) {
                switch (error.status) {
                  case 400:
                    return this.handle400Error(error);
                  case 401:
                    return this.handle401Error(request, next);
                  default:
                    return throwError(error);
                }
              } else {
                return throwError(error);
              }
            })
          );
        }

        return next.handle(request);
      })
    );
  }

  handle400Error(error) {
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
      // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
      return this.logoutUser(error);
    }

    return throwError(error);
  }

  handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);
      this.spotifyTokenService.refreshToken();
    } else {
      return this.tokenSubject.pipe(
        filter(tokens => tokens !== null),
        take(1),
        switchMap(tokens => {
          return next.handle(this.addToken(request, tokens.accessToken));
        })
      );
    }
  }

  logoutUser(error?: any) {
    this.spotifyTokenService.logout();
    return throwError(error ? error : 'LOGOUT');
  }
}
