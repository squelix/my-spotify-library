import { Params } from '@models/interfaces/params';
import { Followers } from '@models/spotify/followers';
import { Image } from '@models/spotify/image';

export class UserPublic {
  display_name: string;
  external_urls: Params;
  followers: Followers;
  href: string;
  id: string;
  images: Image[];
  type: string;
  uri: string;

  constructor(data: object = {}) {
    this.id = data['id'];
    this.display_name = data['display_name'];
    this.external_urls = data['external_urls'];
    this.followers = new Followers(data['followers']);
    this.href = data['href'];
    this.images = (data['images'] || []).map(image => new Image(image));
    this.type = data['type'];
    this.uri = data['uri'];
  }
}
