import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RedirectComponent } from '@features/redirect/redirect/redirect.component';
import { RedirectingComponent } from './redirecting/redirecting.component';
import { RedirectRoutingModule } from '@features/redirect/redirect-routing.module';

@NgModule({
  imports: [CommonModule, RedirectRoutingModule],
  declarations: [RedirectComponent, RedirectingComponent],
})
export class RedirectModule {}
