export enum RedirectRoutingEnum {
  Redirect = 'redirect',
  Redirecting = 'redirecting',
}
