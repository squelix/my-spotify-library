import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { PlaylistsState } from '@store/spotify/playlists/playlists.state';
import { SpotifyState } from '@store/spotify/spotify.state';

@NgModule({
  imports: [CommonModule, NgxsModule.forFeature([SpotifyState, PlaylistsState])],
  declarations: [],
})
export class SpotifyStoreModule {}
