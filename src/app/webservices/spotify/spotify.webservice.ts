import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { buildUrl } from '@commons/utils/url.utils';
import { environment } from '@env/environment';
import { Playlist } from '@models/spotify/playlist';
import { UserPrivate } from '@models/spotify/user-private';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class SpotifyWebservice {
  constructor(private readonly http: HttpClient) {}

  getMyUserInfo(): Observable<UserPrivate> {
    return this.http.get<object>(`${environment.api.spotify}/me`).pipe(map(user => new UserPrivate(user)));
  }

  getMyPlaylists(
    limit = 50,
    offset = 0
  ): Observable<{
    href: string;
    items: Playlist[];
    limit: number;
    next: string;
    offset: number;
    previous: string;
    total: number;
  }> {
    const url = buildUrl(`${environment.api.spotify}/me/playlists`, {
      limit: limit.toString(),
      offset: offset.toString(),
    });

    return this.http
      .get<{
        href: string;
        items: object[];
        limit: number;
        next: string;
        offset: number;
        previous: string;
        total: number;
      }>(url)
      .pipe(
        map(result => {
          return {
            ...result,
            items: (result.items || []).map(item => new Playlist(item)),
          };
        })
      );
  }

  redirectSpotify(): void {
    const url = buildUrl('https://accounts.spotify.com/authorize', {
      response_type: 'token',
      client_id: environment.spotify.clientId,
      scope: 'user-read-private user-read-email',
      redirect_uri: 'http://localhost:4200/spotify/redirect',
    });

    window.open(url, '_self');
  }
}
