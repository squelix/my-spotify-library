import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { RedirectForToken } from '@store/spotify/spotify.actions';

@Component({
  templateUrl: './redirecting.component.html',
  styleUrls: ['./redirecting.component.scss'],
})
export class RedirectingComponent {
  constructor(private readonly store: Store) {
    this.store.dispatch(new RedirectForToken());
  }
}
