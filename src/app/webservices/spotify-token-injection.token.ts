import { InjectionToken } from '@angular/core';
import { SpotifyTokenService } from '@webservices/spotify-token.service';

export const SPOTIFY_TOKEN_SERVICE_TOKEN = new InjectionToken<SpotifyTokenService>('SPOTIFY_TOKEN_SERVICE_TOKEN');
