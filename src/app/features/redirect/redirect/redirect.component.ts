import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Init } from '@store/spotify/spotify.actions';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss'],
})
export class RedirectComponent implements OnInit {
  constructor(private readonly route: ActivatedRoute, private readonly store: Store) {}

  ngOnInit(): void {
    this.route.fragment
      .pipe(
        filter(fragments => {
          const urlParams = new URLSearchParams(fragments);
          return !!urlParams.get('access_token');
        }),
        take(1)
      )
      .subscribe(fragments => {
        const urlParams = new URLSearchParams(fragments);
        this.store.dispatch(new Init(urlParams.get('access_token')));
      });
  }
}
