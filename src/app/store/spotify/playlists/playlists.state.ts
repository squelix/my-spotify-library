import { Playlist } from '@models/spotify/playlist';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  GetMyPlaylists,
  GetMyPlaylistsFailed,
  GetMyPlaylistsSuccess,
} from '@store/spotify/playlists/playlists.actions';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';
import { catchError, switchMap } from 'rxjs/operators';

export interface PlaylistsStateModel {
  items: Playlist[];
  limit: number;
  next: string;
  offset: number;
  previous: string;
  total: number;
  error: any;
}

const defaultState: PlaylistsStateModel = {
  items: [],
  limit: 50,
  offset: 0,
  next: null,
  total: null,
  previous: null,
  error: null,
};

@State<PlaylistsStateModel>({
  name: 'playlists',
  defaults: defaultState,
})
export class PlaylistsState {
  @Selector()
  static playlists(state: PlaylistsStateModel) {
    return state.items;
  }
  constructor(private readonly spotifyWebservice: SpotifyWebservice) {}

  @Action(GetMyPlaylists)
  getMyPlaylists(ctx: StateContext<PlaylistsStateModel>) {
    const state = ctx.getState();

    return this.spotifyWebservice.getMyPlaylists(state.limit, state.offset).pipe(
      switchMap(result =>
        ctx.dispatch(
          new GetMyPlaylistsSuccess(
            result.items,
            result.limit,
            result.next,
            result.offset,
            result.previous,
            result.total
          )
        )
      ),
      catchError(error => ctx.dispatch(new GetMyPlaylistsFailed(error)))
    );
  }

  @Action(GetMyPlaylistsSuccess)
  getMyPlaylistsSuccess(ctx: StateContext<PlaylistsStateModel>, action: GetMyPlaylistsSuccess) {
    const { items, total, previous, offset, next, limit } = action;

    ctx.patchState({ items, total, previous, offset, next, limit });
  }

  @Action(GetMyPlaylistsFailed)
  getMyPlaylistsError(ctx: StateContext<PlaylistsStateModel>, action: GetMyPlaylistsFailed) {
    ctx.patchState({ error: action.error });
  }
}
