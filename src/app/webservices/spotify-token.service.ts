import { from, Observable } from 'rxjs';
import { SpotifyTokens } from '@models/spotify-tokens';
import { ContextHolderService } from '@core/services/context-holder.service';
import { RedirectForToken } from '@store/spotify/spotify.actions';
import { Store } from '@ngxs/store';

export class SpotifyTokenService {
  blacklistRoutes: string[] = [];

  constructor(private readonly contextHolder: ContextHolderService, private readonly store: Store) {}

  getTokens(): Observable<SpotifyTokens> {
    return this.contextHolder.tokens$;
  }

  refreshToken(): void {
    this.store.dispatch(new RedirectForToken());
  }

  saveLocalTokens(tokens: SpotifyTokens): void {
    this.contextHolder.tokens$ = from([tokens]);
  }

  logout(): void {
    console.log('logout');
  }
}
