import { Component } from '@angular/core';
import { buildPath } from '@commons/utils/routing.utils';
import { ContextHolderService } from '@core/services/context-holder.service';
import { FeaturesRoutingEnum } from '@features/features-routing.enum';
import { faAngular, faSpotify } from '@fortawesome/free-brands-svg-icons';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { filter, take } from 'rxjs/operators';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  faSpotify = faSpotify;
  faAngular = faAngular;

  constructor(private readonly contextHolder: ContextHolderService, private readonly store: Store) {
    this.contextHolder.tokens$
      .pipe(
        filter(tokens => !!(tokens && tokens.accessToken)),
        take(1)
      )
      .subscribe(tokens => {
        if (tokens && tokens.accessToken) {
          this.store.dispatch(new Navigate([buildPath(FeaturesRoutingEnum.Library)]));
        }
      });
  }
}
