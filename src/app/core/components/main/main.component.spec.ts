import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from '@core/components/header/header.component';
import { MainComponent } from '@core/components/main/main.component';
import { SpriteComponent } from '@core/components/sprite/sprite.component';
import { ContextHolderService } from '@core/services/context-holder.service';
import { DataStorageService } from '@core/services/data-storage.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxsModule } from '@ngxs/store';
import { PlaylistsState } from '@store/spotify/playlists/playlists.state';
import { SpotifyState } from '@store/spotify/spotify.state';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';

jest.mock('@core/services/context-holder.service');
jest.mock('@core/services/data-storage.service');

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FontAwesomeModule,
        NgxsModule.forRoot([SpotifyState, PlaylistsState]),
        HttpClientTestingModule,
      ],
      declarations: [MainComponent, SpriteComponent, HeaderComponent],
      providers: [DataStorageService, ContextHolderService, SpotifyWebservice],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should be created', () => {
    expect(component).toBeTruthy();
  });

  test('should have class `hover-on` by default', () => {
    expect(fixture.nativeElement.classList.contains('hover-on')).toBe(true);
  });

  test('should not have class `focus-off` by default', () => {
    expect(fixture.nativeElement.classList.contains('focus-off')).toBe(false);
  });

  test('should have class `hover-on` after mouse pointer event', () => {
    component.hoverEnabled = false;
    component.onPointerEnter({ pointerType: 'mouse' } as any); // `PointerEvent` constructor not supported by Jest
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('hover-on')).toBe(true);
  });

  test('should not have class `hover-on` after non-mouse pointer event', () => {
    component.hoverEnabled = true;
    component.onPointerEnter({ pointerType: 'touch' } as any); // `PointerEvent` constructor not supported by Jest
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('hover-on')).toBe(false);
  });

  test('should not have class `hover-on` after touch event', () => {
    component.hoverEnabled = true;
    fixture.nativeElement.dispatchEvent(new TouchEvent('touchstart'));
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('hover-on')).toBe(false);
  });

  test('should have class `focus-off` after mouse event', () => {
    component.focusOutlineDisabled = false;
    fixture.nativeElement.dispatchEvent(new MouseEvent('mousedown'));
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('focus-off')).toBe(true);
  });

  test('should not have class `focus-off` after tab key event', () => {
    component.focusOutlineDisabled = true;
    fixture.nativeElement.dispatchEvent(new KeyboardEvent('keydown', { key: 'Tab' }));
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('focus-off')).toBe(false);
  });

  test('should not toggle class `focus-off` after non-tab key event', () => {
    component.focusOutlineDisabled = false;
    fixture.nativeElement.dispatchEvent(new KeyboardEvent('keydown'));
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('focus-off')).toBe(false);

    component.focusOutlineDisabled = true;
    fixture.nativeElement.dispatchEvent(new KeyboardEvent('keydown'));
    fixture.detectChanges();

    expect(fixture.nativeElement.classList.contains('focus-off')).toBe(true);
  });
});
