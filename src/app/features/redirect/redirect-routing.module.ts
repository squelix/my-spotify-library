import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedirectRoutingEnum } from '@features/redirect/redirect-routing.enum';
import { RedirectComponent } from '@features/redirect/redirect/redirect.component';
import { RedirectingComponent } from '@features/redirect/redirecting/redirecting.component';

const routes: Routes = [
  {
    path: RedirectRoutingEnum.Redirect,
    component: RedirectComponent,
  },
  {
    path: RedirectRoutingEnum.Redirecting,
    component: RedirectingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RedirectRoutingModule {}
