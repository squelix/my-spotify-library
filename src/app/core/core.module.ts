import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from '@core/components/main/main.component';
import { SpriteComponent } from '@core/components/sprite/sprite.component';
import { CoreRoutingModule } from '@core/core-routing.module';
import { environment } from '@env/environment';
import { FeaturesModule } from '@features/features.module';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsModule } from '@ngxs/store';
import { HeaderComponent } from './components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DataStorageService } from '@core/services/data-storage.service';
import { DataStorage } from '@models/interfaces/data-storage.interface';
import { ContextHolderService } from '@core/services/context-holder.service';
import { SpotifyTokenInterceptor } from '@webservices/spotify-token.interceptor';
import { SpotifyTokenService } from '@webservices/spotify-token.service';
import { SPOTIFY_TOKEN_SERVICE_TOKEN } from '@webservices/spotify-token-injection.token';

@NgModule({
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FeaturesModule,
    CoreRoutingModule,
    FontAwesomeModule,

    NgxsModule.forRoot([]),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot({
      disabled: environment.prod,
    }),

    // This import must always be at the end
    NgxsLoggerPluginModule.forRoot({
      disabled: !environment.dev,
    }),
  ],
  declarations: [MainComponent, SpriteComponent, HeaderComponent],
  exports: [HeaderComponent],
  providers: [
    { provide: DataStorage, useClass: DataStorageService },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpotifyTokenInterceptor,
      multi: true,
    },
    {
      provide: SPOTIFY_TOKEN_SERVICE_TOKEN,
      useClass: SpotifyTokenService,
    },
    ContextHolderService,
  ],
  bootstrap: [MainComponent],
})
export class CoreModule {}
