import * as qs from 'qs';

export interface Params {
  [key: string]: string;
}

function filterKeys(params: Params, filterPredicate: (param: string) => boolean): Params {
  const filteredParams: Params = {};

  return Object.keys(params)
    .filter(filterPredicate)
    .reduce((carry, it) => {
      carry[it] = params[it];
      return carry;
    }, filteredParams);
}

function isParamInPath(path: string, param: string): boolean {
  return new RegExp(`\\??:${param}`).test(path);
}

function replacePathParams(path: string, params: Params) {
  return path.replace(/\/\??:[^\/]+/g, it => {
    const isOptional = it.charAt(1) === '?';
    const paramName = it.substr((isOptional ? '/?:' : '/:').length);

    if (params[paramName]) {
      return `/${params[paramName]}`;
    }
    if (isOptional) {
      return '';
    }

    throw new Error(`missing parameter ${paramName}`);
  });
}

export function buildUrl(path: string, params: Params = {}) {
  const pathParams = filterKeys(params, param => isParamInPath(path, param));
  const qsParams = filterKeys(params, param => !isParamInPath(path, param));

  const finalPath = replacePathParams(path, pathParams);
  const queryString = qs.stringify(qsParams);

  // tslint:disable:no-nested-template-literals
  return `${finalPath}${queryString ? `?${queryString}` : ''}`;
}
