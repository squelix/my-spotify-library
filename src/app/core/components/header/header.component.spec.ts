import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HeaderComponent } from '@core/components/header/header.component';
import { ContextHolderService } from '@core/services/context-holder.service';
import { DataStorageService } from '@core/services/data-storage.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxsModule } from '@ngxs/store';
import { PlaylistsState } from '@store/spotify/playlists/playlists.state';
import { SpotifyState } from '@store/spotify/spotify.state';
import { SpotifyWebservice } from '@webservices/spotify/spotify.webservice';

jest.mock('@core/services/context-holder.service');
jest.mock('@core/services/data-storage.service');

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FontAwesomeModule,
        NgxsModule.forRoot([SpotifyState, PlaylistsState]),
        HttpClientTestingModule,
      ],
      declarations: [HeaderComponent],
      providers: [DataStorageService, ContextHolderService, SpotifyWebservice],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
