import { SpotifyTokens } from '@models/spotify-tokens';
import { from, Observable } from 'rxjs';

export class ContextHolderService {
  tokens$: Observable<SpotifyTokens> = from([new SpotifyTokens({})]);
}
