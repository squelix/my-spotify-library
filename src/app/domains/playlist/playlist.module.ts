import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PlaylistComponent } from '@domains/playlist/playlist/playlist.component';

@NgModule({
  imports: [CommonModule],
  declarations: [PlaylistComponent],
  exports: [PlaylistComponent],
})
export class PlaylistModule {}
