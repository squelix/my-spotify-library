import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LibraryComponent } from '@features/library/library/library.component';
import { UserResolver } from '@webservices/spotify/user.resolver';

const routes: Routes = [
  {
    path: '',
    resolve: {
      user: UserResolver,
    },
    component: LibraryComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LibraryRoutingModule {}
