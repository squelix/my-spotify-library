export class Image {
  height: number;
  url: string;
  width: number;

  constructor(data: object = {}) {
    this.height = data['height'];
    this.url = data['url'];
    this.width = data['width'];
  }
}
