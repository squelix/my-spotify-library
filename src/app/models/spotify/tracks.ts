export class Tracks {
  href: string;
  total: number;

  constructor(data: object = {}) {
    this.href = data['href'];
    this.total = data['total'];
  }
}
